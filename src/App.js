import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3>
          Ingeniería de Software
        </h3>
        <p>
          Esta es un app muy basica aún
        </p>
        <a
          className="App-link"
          href="https://www.linkedin.com/in/maxivalenzano/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Hello world
        </a>
      </header>
    </div>
  );
}

export default App;
