import { render, screen } from '@testing-library/react';
import App from './App';

test('title-check', () => {
  render(<App />);
  const texto = screen.getByText(/Esta es un app muy basica aún/i);
  expect(texto).toBeInTheDocument();
});

test('hello-world-check', () => {
  render(<App />);
  const linkElement = screen.getByText(/hello world/i);
  expect(linkElement).toBeInTheDocument();
});
