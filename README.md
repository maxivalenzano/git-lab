# Getting Started with Create React App
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Available Scripts
In the project directory, you can run:
### `yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`
Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `Pasos que seguí`

Crear el proyecto con create-react-app
Iniciarlizar todo el proceso de git, con 2 ramas, main y development
Registrarse en Netlify, que es un servidor de hosting para aplicacion de React
Luego instalar netlify-cli globalmente "npm i netlify-cli -g"
una vez instalado, se prepara para el repositorio en netlify.
Ejecutar "netlify init --manual" y le pedirá que le deen acceso en el navegador, luego en la consola le dan crear nuevo proyecto y el nombre
La Site ID tenemos que guardar para mas tarde
